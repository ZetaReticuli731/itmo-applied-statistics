from math import *
from decimal import *


n = int(input('Введите количество испытаний (n): '))
p = float(input('Введите вероятность успеха в одном испытании (p): '))
expression = input('Введите выражение, вероятность которого необходимо подсчитать, без пробелов, вместо "\u03B5" используйте латинскую "e".\nНапример: 3<=e<9 или e>=6.\nВыражение: ')


index = expression.find('e')
if index == -1:
	print('Вы ошиблись при вводе данных :с')
elif index == 0:
	if expression[1] == '>':
		k2 = n + 1
		if expression[2] == '=':
			k1 = int(expression[3:])
		else:
			k1 = int(expression[2:]) + 1
	else:
		k1 = 0
		if expression[2] == '=':
			k2 = int(expression[3:]) + 1
		else:
			k2 = int(expression[2:])
else:
	if expression[index - 1] == '=':
		k1 = int(expression[:index - 2])
	else:
		k1 = int(expression[:index - 1]) + 1
	if expression[index + 2] == '=':
		k2 = int(expression[index + 3:]) + 1
	else:
		k2 = int(expression[index + 2:])


s = 0
for k in range(k1, k2):
	s += p ** k * (1 - p) ** (n - k) / factorial(k) / factorial(n - k)
print('Итоговая вероятность: ' + str(Decimal(factorial(n) * s).quantize(Decimal('0.001'))))